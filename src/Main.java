import java.io.*;

public class Main {
    public static void main(String[] args) {
//        try(Stream<String> lines = Files.lines(Path.of("resources/in.txt"))) {
//            lines.skip(1).forEach(line -> {
//                System.out.println(line);
//            });
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        try (FileOutputStream out = new FileOutputStream("resources/out.txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(out)) {

            Car car = new Car("Toyota", "key1234", "Camry", 2020, "Black");
            objectOutputStream.writeObject(car);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (FileInputStream in = new FileInputStream("resources/out.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(in)) {

            Car car = (Car) objectInputStream.readObject();
            System.out.println(car);

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}