import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RAF {
    public static void main(String[] args) throws FileNotFoundException {
        Car car = new Car("Toyota", "key1234", "Camry", 2020, "Black");
        writeCar(car);
        readCar();
    }

    //    private String make;  15 Bytes
//    private transient String key;  15 Bytes
//    private String model;  15 Bytes
//    private int year;  4 Bytes
//    private String color;   15 Bytes
    private static void writeCar(Car car) throws FileNotFoundException {
        try (RandomAccessFile raf = new RandomAccessFile("resources/out.bin", "rw")) {
            raf.setLength(0);
//            if (car.getMake().length() > 15) {
//                throw new IllegalArgumentException("Make is too long");
//            }
            raf.writeBytes(String.format("%-15s", car.getMake()));
//            raf.writeBytes(car.getMake());

            raf.writeBytes(String.format("%-15s", car.getModel()));
//            raf.writeBytes(car.getModel());

            raf.writeInt(car.getYear());

            raf.writeBytes(String.format("%-15s", car.getColor()));
//            raf.writeBytes(car.getColor());

//            raf.writeUTF(car.getMake());
//            raf.writeUTF(car.getModel());
//            raf.writeInt(car.getYear());
//            raf.writeUTF(car.getColor());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void readCar() throws FileNotFoundException {
        try (RandomAccessFile raf = new RandomAccessFile("resources/out.bin", "r")) {
            byte[] makeBytes = new byte[15];
            raf.read(makeBytes);
            byte[] modelBytes = new byte[15];
            raf.read(modelBytes);
            int year = raf.readInt();
            byte[] colorBytes = new byte[15];
            raf.read(colorBytes);
            System.out.println(new String(makeBytes).trim() + " " + new String(modelBytes).trim() + " " + year + " " + new String(colorBytes).trim());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
